#include "../headers/Administrateur.h"

Administrateur::Administrateur(string id, string mdp) {
    this->id = id;
    this->mdp = mdp;
}

Administrateur::~Administrateur() {
}

string Administrateur::getId() const{
    return this->id;
}

void Administrateur::setId(const string id){
    this->id = id;
}

string Administrateur::getMdp() const{
    return this->mdp;
}

void Administrateur::setMdp(const string mdp){
    this->mdp = mdp;
}

/**
 * @brief Permet d'ajouter un vol à la map
 * 
 * @param vols : map de vol, modifiée en sortie
 * @throws char* : exception si la date saisie est antérieure à la date actuelle (de la machine)
 */
void Administrateur::ajouterVol(map<long, Vol*> * vols){
    Vol* vol;
    long num;
    int places, jour, mois, annee, heure, min;
    string depart, arrivee;
    float prix;
    Destination* dest;
    Date* date;

    cout << "Veuillez saisir les informations du vol à ajouter :" << endl;
    cout << "Numéro de vol :" << endl;
    cin >> num;
    cout << "Nombre de places maximales :" << endl;
    cin >> places;
    cout << "Ville de départ :" << endl;
    cin >> depart;
    cout << "Ville d'arrivée :" << endl;
    cin >> arrivee;
    // Création de la destination
    dest = new Destination(depart, arrivee);
    cout << "Date :" << endl; 
    cin >> jour;
    cout << "/";
    cin >> mois;
    cout << "/";
    cin >> annee;
    cout << endl << "Heure : " << endl;
    cin >> heure;
    cout << ":";
    cin >> min;
    cout << endl << "Prix :" << endl;
    date = new Date(jour, mois, annee, heure, min);

    if (!date->estUneDatePosterieure()) {
        throw "Erreur ! La date est antérieure...";
    }

    cin >> prix;
    vol = new Vol(num, places, dest, date, prix);
    (*vols)[num] = vol;
}

/**
 * @brief Permet de modifier la date / heure d'un vol dont l'id doit être saisi manuellement
 * 
 * @param vols : map de tous les vols
 */
void Administrateur::modifierDateHeureVol(map<long, Vol*> *vols) {
    cout << "De quel vol voulez-vous modifier la date / heure ?" << endl;
    afficherVols(*vols);
    cout << "Veuillez saisir son id : ";
    long id;
    cin >> id;
    if (vols->find(id) == vols->end()) {
        cerr << "Vol non existant..." << endl;
    } else {
        cout << "Date actuelle : " << (*vols)[id]->getDate()->toString() << endl;
        int  jour, mois, annee, heure, min;
        cout << "Veuillez saisir la nouvelle date : " << endl;
        cin >> jour;
        cout << "/";
        cin >> mois;
        cout << "/";
        cin >> annee;
        cout << endl << "Heure : " << endl;
        cin >> heure;
        cout << ":";
        cin >> min;
        Date *date = new Date(jour, mois, annee, heure, min);
        (*vols)[id]->setDate(date);

        cout << "La date a bien été modifiée." << endl;

    }

}

/**
 * @brief Permet d'afficher la liste des passagers par vol
 * 
 * @param passagers 
 * @param vols 
 */
void Administrateur::afficherPassagersParVols(map<long, Passager*> passagers, map<long, Vol*> vols) const {
    // Itérateur sur les vols
    map<long, Vol*>::iterator itVols;
    // Itérateur sur les passagers
    map<long, Passager*>::iterator itPassagers;

    itVols = vols.begin();
    cout << "Liste des vols enregistrés :" << endl << endl;
    while (itVols != vols.end()) {
        cout << "Numéro vol : ";
        cout << itVols->second->getNumVol() << endl;
        cout << "Nombre places max : ";
        cout << itVols->second->getNbPlacesMax() << endl;
        cout << "Ville de départ : ";
        cout << itVols->second->getDestination()->getVilleDepart() << endl;
        cout << "Ville d'arrivée : ";
        cout << itVols->second->getDestination()->getVilleArrive() << endl;
        cout << "Date : ";
        cout << itVols->second->getDate()->toString() << endl;
        cout << "Prix : ";
        cout << itVols->second->getPrix() << endl;

        cout << "Liste passagers : " << endl;

        // On regarde parmi les passagers
        for (itPassagers = passagers.begin(); itPassagers != passagers.end(); itPassagers++) {

            // On regarde parmi leurs réservations
            for (size_t i = 0; i < itPassagers->second->getReservations().size(); i++) {

                // On affiche ceux qui ont des réservation sur le vol sur lequel on itère
                if (itPassagers->second->getReservations()[i]->getNumVol() == itVols->second->getNumVol()) {
                    cout << itPassagers->second->afficher() << endl;
                }
            }
        }

        cout << endl << endl;

        itVols++;
    }
    cout << endl << "Fin de la liste des vols enregistrés :" << endl;
}

/**
 * @brief Permet de tester l'egalité de deux Administrateurs
 * 
 * @param admin : l'autre administrateur
 * @return true : id ET mdp sont égaux
 * @return false : sinon
 */
bool Administrateur::operator==(const Administrateur &admin) const {
    return this->id == admin.getId() && this->mdp == admin.getMdp();
}
