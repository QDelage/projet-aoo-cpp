#include "../headers/Menus.h"
#include "../headers/DB.h"
#include "../headers/Reservation.h"
#include <map>

using namespace std;

/**
 * Menu de l'administrat·eur·rice
 * Retourne 0 si tout se passe bien
 */
int menuAdmin(){
    // On stock les vols dans une hashmap avant de les écrire SI l'admin veut enregistrer
    map<long, Vol*> vols;
    // Permettra de récupérer la liste des vols de la DB avant de la réécrire
    vector<Vol*> listeVols;

    map<long, Passager*> passagers;

    passagers = DataBase::listePassagers();

    Passager *pgr;

    // Choix du menu
    string choix;

    Administrateur adm = Administrateur::connexion();
    if (adm == Administrateur("", "")) {
        return 1;
    }

    bool quit = false;

    listeVols = DataBase::listVol();
    // On récupère d'abord le contenu du fichier
    // Ainsi, on aura tout dans notre hashmap qui sera trié selon le numéro du vol (ordre croissant)
    for (long unsigned int i = 0; i < listeVols.size(); i++) {
        vols[listeVols[i]->getNumVol()] = listeVols[i];
    }

    while (!quit) {
        cout << "-----------------Choix-----------------" << endl;
        cout << "0 - Quitter et sauvegarder" << endl;
        cout << "1 - Quitter sans sauvegarder" << endl << endl;

        cout << "2 - Modifier la date / heure d'un vol" << endl;
        cout << "3 - Afficher liste passagers vols" << endl;
        cout << "4 - Ajouter Vol" << endl;
        cout << "5 - Afficher la liste des vols" << endl;
        cout << "6 - Ajouter passager" << endl;
        cout << "7 - Vérifier l'existence d'un vol" << endl;
        cout << "---------------------------------------" << endl;

        int menu = 0;
        cin >> menu;

        switch (menu){
            case 0:
                DataBase::sauvegarderVols(vols);
                DataBase::sauvegarderPassagers(passagers);
                return 0;
                break;

            case 1:
                return 0;
                break;

            case 2:
                adm.modifierDateHeureVol(&vols);
                break;

            case 3:
                adm.afficherPassagersParVols(passagers, vols);
                break;

            case 4:
                // On gère l'exception de la date postérieure
                try{
                    adm.ajouterVol(&vols);
                } catch(const char* e) {
                    std::cerr << e << endl << "Vol non enregistré" << endl;
                }
                break;

            case 5:
                adm.afficherVols(vols);
                break;

            case 6:
                pgr = DataBase::creerPassager();
                passagers[pgr->getNumPasseport()] = pgr;
                break;

            case 7:
                adm.verifierExistenceVol(vols);
                break;

            default:
                cerr << "def" << endl;
                break;
        }

    }

    return 0;
}

/**
 * Menu de l'utilisat·eur·rice
 * Retourne 0 si tout se passe bien
 */
int menuUser(){
    // On stock les vols dans une hashmap avant de les écrire SI l'admin veut enregistrer
    map<long, Vol*> vols;
    map<long, Reservation*> reservations;
    vector<Reservation*> listeRes;
    vector<Vol*> listeVols;

    listeVols = DataBase::listVol();
    // On récupère d'abord le contenu du fichier
    // Ainsi, on aura tout dans notre hashmap qui sera trié selon le numéro du vol (ordre croissant)
    for (long unsigned int i = 0; i < listeVols.size(); i++) {
        vols[listeVols[i]->getNumVol()] = listeVols[i];
    }

    Passager *pgr = DataBase::choixPassager();

    if (NULL == pgr) {
        return 1;
    }

    listeRes = DataBase::listeReservations();
    for (long unsigned int i = 0; i < listeRes.size(); i++) {
        reservations[listeRes[i]->getNumReservation()] = listeRes[i];

        // On ajoute à notre passager ses réservations
        if(listeRes[i]->getNumPasseport() == pgr->getNumPasseport()) {
            pgr->ajouterReservation(listeRes[i]);
        }
    }

    bool quit = false;
    string choix;


    while (!quit) {
        cout << "-------------------Choix-------------------" << endl;
        cout << "0 - Quitter et sauvegarder" << endl;
        cout << "1 - Quitter sans sauvegarder" << endl << endl;

        cout << "2 - Réserver un vol" << endl;
        cout << "3 - Confirmer une réservation" << endl;
        cout << "4 - Annuler une réservation" << endl;
        cout << "5 - Afficher la liste des réservations" << endl;
        cout << "6 - Afficher la liste des vols" << endl;
        cout << "7 - Vérifier l'existence d'un vol" << endl;
        cout << "8 - Vérifier l'existence d'une réservation" << endl;
        cout << "-------------------------------------------" << endl;

        int menu = 0;

        cin >> menu;

        switch (menu){
            case 0:
                DataBase::resetReservations();
                // On itère sur tous les éléments de 'reservations', on ajoute toutes les reservations qu'elle contient
                for (map<long, Reservation*>::iterator it = reservations.begin(); it != reservations.end(); it++) {
                    DataBase::ajouterReservation(it->second);
                }

                delete pgr;
                return 0;

            case 1:
                delete pgr;
                return 0;

            case 2:
                pgr->reserverVol(vols, reservations);
                break;

            case 3:
                pgr->confirmerReservation(reservations);
                break;

            case 4:
                pgr->annulerReservation(reservations);
                break;

            case 5:
                pgr->listerReservations();
                break;

            case 6:
                pgr->afficherVols(vols);
                break;

            case 7:
                pgr->verifierExistenceVol(vols);
                break;

            case 8:
                pgr->verifierExistenceReservation(reservations);
                break;

            default:
                cerr << "Erreur de saisie" << endl;
                break;
        }
    }

    delete pgr;

    return 0;
}