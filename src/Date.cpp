#include "../headers/Date.h"
#include <sstream>

Date::Date(int jour, int mois, int annee, int heure, int min) {
    // On renvoie des exceptions selon le champ qui contient une erreur
    if (jour < 1 || jour > 31) {
        throw 1;
    }

    if (mois < 1 || mois > 12) {
        throw 2;
    }

    // L'an 0 n'existe pas en histoire
    if (annee == 0) {
        throw 3;
    }

    if (min < 1 || min > 60) {
        throw 4;
    }

    if (heure < 1 || heure > 24) {
        throw 5;
    }
    
    this->jour = jour;
    this->mois = mois;
    this->annee = annee;
    this->heure = heure;
    this->min = min;
}

Date::~Date() {
}

int Date::getJour() const {
    return this->jour;
}

void Date::setJour(const int jour){
    this->jour = jour;
}

int Date::getMois() const {
    return this->mois;
}

void Date::setMois(const int mois){
    this->mois = mois;
}

int Date::getAnnee() const {
    return this->annee;
}

void Date::setAnnee(const int annee){
    this->annee = annee;
}

int Date::getMin() const {
    return this->min;
}

void Date::setMin(const int min){
    this->min = min;
}

int Date::getHeure() const {
    return this->heure;
}

void Date::setHeure(const int heure){
    this->heure = heure;
}

string Date::toString() const {
    stringstream str;
    str << this->jour << "/" << this->mois << "/" << this->annee;
    str << " ";
    str << this->heure << ":" << this->min;

    return str.str();
}

/**
 * @brief Permet de savoir si une date est bien postérieure à la date actuelle
 * 
 * @return true : elle est dans le futur
 * @return false : elle est passée
 */
bool Date::estUneDatePosterieure() const {
    time_t     now = time(0);
    struct tm  tstruct;
    tstruct = *localtime(&now);

    bool valide = true;
    
    // On vérifie si la date est bien postérieure à la date actuelle

    // Les années sont stockées selon l'année 1900
    if (annee - 1900 < tstruct.tm_year) {
        valide = false;
    } else if (annee - 1900 == tstruct.tm_year) {
        if (mois - 1 < tstruct.tm_mon) {
            valide = false;
        } else if (mois - 1 == tstruct.tm_mon) {
            if (jour < tstruct.tm_mday) {
                valide = false;
            }
            else if (jour == tstruct.tm_mday) {
                if (heure < tstruct.tm_hour) {
                    valide = false;
                } else if (heure == tstruct.tm_hour) {
                    if (min < tstruct.tm_min) {
                        valide = false;
                    } 
                }
                
            }
        }
    }

    return valide;
}