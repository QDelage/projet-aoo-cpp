#include "../headers/DB.h"
#include "../headers/Administrateur.h"

/**
 * @brief Permet d'obtenir la liste de tous les admins de la DB
 * 
 * @return vector<Administrateur> : liste des admins autorisés
 */
vector<Administrateur> DataBase::getAllAdmins() {
    vector<Administrateur> admins;

    const int sz = 100;
    char buff[sz];
    ifstream in("DB/admins.txt");

    string str, str2;

    // Pour chaque ligne du fichier texte, on va créer un vol et l'ajouter au vecteur
    while (in.get(buff, sz)) {
        in.get();
        str = buff;
        size_t start, end;
        end = 0;
        string out[10];
        int i = 0;

        // Permet de mettre dans le vecteur de string tout ce qui est entre ';'
        while ((start = str.find_first_not_of(':', end)) != std::string::npos) {
            end = str.find(':', start);
            out[i] = (str.substr(start, end - start));
            i++;
        }

        // Utilise le vecteur de strings pour créer un Administrateur
        Administrateur adm(out[0], out[1]);
        admins.push_back(adm);
    }
    
    in.close();

    // Retourne la liste des vols
    return admins;
}

/**
 * @brief Permet de récupérer les vols existants
 * 
 * @return vector<Vol*> : vols de la DB
 */
vector<Vol*> DataBase::listVol(){
    const int sz = 100;
    char buff[sz];
    ifstream in("DB/vols.txt");
    vector<Vol*> vols;

    string str, str2;

    // Pour chaque ligne du fichier texte, on va créer un vol et l'ajouter au vecteur
    while (in.get(buff, sz)) {
        in.get();
        str = buff;
        size_t start, end;
        end = 0;
        string out[10];
        int i = 0;

        // Permet de mettre dans le vecteur de string tout ce qui est entre ';'
        while ((start = str.find_first_not_of(';', end)) != std::string::npos) {
            end = str.find(';', start);
            out[i] = (str.substr(start, end - start));
            i++;
        }

        // Utilise le vecteur de strings pour créer un vol
        string num, nbPlaces, villeD, villeA, j, M, A, h, m, prix;
        Destination *dest = new Destination(out[2], out[3]);
        Date *date = new Date(stoi(out[4]), stoi(out[5]), stoi(out[6]), stoi(out[7]), stoi(out[8]));
        Vol *vol = new Vol(stol(out[0]), stoi(out[1]), dest, date, stof(out[9]));
        vols.push_back(vol);
    }
    
    in.close();

    // Retourne la liste des vols
    return vols;

}

/**
 * @brief Ajoute un vol à la suite du fichier
 * 
 * @param vol
 */
void DataBase::ajouterVol(Vol *vol){
    // Ouvre le fichier en écriture, se positionne à la fin du fichier
    ofstream out("DB/vols.txt", ios::app);
    
    stringstream str;
    str << vol->getNumVol() << ";";
    str << vol->getNbPlacesMax() << ";";
    str << vol->getDestination()->getVilleDepart() << ";";
    str << vol->getDestination()->getVilleArrive() << ";";
    str << vol->getDate()->getJour() << ";";
    str << vol->getDate()->getMois() << ";";
    str << vol->getDate()->getAnnee() << ";";
    str << vol->getDate()->getHeure() << ";";
    str << vol->getDate()->getMin() << ";";
    str << vol->getPrix();

    out << str.str() << endl;

}

/**
 * @brief Écrase le fichier actuel avec les infos des vols de la map
 * 
 * @param vols 
 */
void DataBase::sauvegarderVols(map<long, Vol*> vols) {
    DataBase::resetVols();
    // On itère sur tous les éléments de 'Vols', on ajoute tous les vols qu'elle contient
    for (map<long, Vol*>::iterator it = vols.begin(); it != vols.end(); it++) {
        DataBase::ajouterVol(it->second);
    }
}

/**
 * @brief Efface le fichier vols, pour pouvoir le reprendre à 0 avec les nouvelles données
 * 
 */
void DataBase::resetVols(){
    // Efface le fichier pour le reprendre à 0
    remove("DB/vols.txt");
}

/**
 * @brief Permet de récupérer un vecteur des passagers existants en DB
 * 
 * @return vector<Passager*> 
 */
vector<Passager*> DataBase::listPassagers(){
    const int sz = 100;
    char buff[sz];
    ifstream in("DB/passagers.txt");
    vector<Passager*> passagers;

    string str, str2;

    // Pour chaque ligne du fichier texte, on va créer un passager et l'ajouter au vecteur
    while (in.get(buff, sz)) {
        in.get();
        str = buff;
        size_t start, end;
        end = 0;
        string out[10];
        int i = 0;

        // Permet de mettre dans le vecteur de string tout ce qui est entre ';'
        while ((start = str.find_first_not_of(';', end)) != std::string::npos) {
            end = str.find(';', start);
            out[i] = (str.substr(start, end - start));
            i++;
        }

        // Utilise le vecteur de strings pour créer un passager
        string nom, prenom, titre;
        Passager *passager = new Passager(out[0], out[1], stoi(out[2]), out[3], stol(out[4]));
        passagers.push_back(passager);
    }
    
    in.close();

    // Retourne la liste des passagers
    return passagers;

}

/**
 * @brief Permet de récupérer une map des passagers existants en DB
 * 
 * @return map<long, Passager*> 
 */
map<long, Passager*> DataBase::listePassagers() {
    vector<Passager*> passagers = DataBase::listPassagers();
    map<long, Passager*> pgrs;

    for (size_t i = 0; i < passagers.size(); i++) {
        passagers[i]->setReservations(DataBase::getReservationsParPassager(passagers[i]->getNumPasseport()));
        pgrs[passagers[i]->getNumPasseport()] = passagers[i];
    }

    return pgrs;
}

/**
 * @brief Récupère les réservations d'un passager depuis la DB
 * 
 * @param numPasseport : ID du passager
 * @return vector<Reservation*> 
 */
vector<Reservation*> DataBase::getReservationsParPassager(long numPasseport) {
    vector<Reservation*> reservations = DataBase::listeReservations();
    vector<Reservation*> reservationsPassager = DataBase::listeReservations();

    for (size_t i = 0; i < reservations.size(); i++) {
        if (reservations[i]->getNumPasseport() == numPasseport) {
            reservationsPassager.push_back(reservations[i]);
        }
    }

    return reservationsPassager;
}

/**
 * @brief Permet de choisir son passager parmi la DB
 * S'il n'éxiste pas, retourne NULL
 * 
 * @return Passager* 
 */
Passager* DataBase::choixPassager() {
    vector<Passager*> liste = DataBase::listPassagers();

    for (long unsigned int i = 0; i < liste.size(); i++) {
        cout << i+1 << " : " << liste[i]->getNom() << " " << liste[i]->getPrenom() << endl;
    }

    long unsigned int j;
    cout << "Quelle personne êtes-vous ?" << endl;
    cin >> j;
    
    if (j < 1 || j > liste.size()) {
        cerr << "Erreur de saisie..." << endl;
        return NULL;
    } else {
        return liste[j-1];
    }
    
}

/**
 * @brief Permet de créer un passager.
 * Demande ses informations en prompt.
 * Écrit automatiquement ses infos dans la DB.
 * 
 * @return Passager* : Pointeur vers le nouveau passager
 */
Passager* DataBase::creerPassager() {
    string nom, prenom, titre;
    int age;
    long passeport;
    cout << "Quel est vôtre nom ? " << endl;
    cin >> nom;
    cout << "Votre prénom ? " << endl;
    cin >> prenom;
    cout << "Votre titre ? " << endl;
    cin >> titre;
    cout << "Votre âge ? " << endl;
    cin >> age;
    cout << "Votre numéro de passeport ? " << endl;
    cin >> passeport;

    Passager *pgr = new Passager(nom, prenom, age, titre, passeport);

    return pgr;

}

/**
 * @brief Ajoute un à un les passagers au fichier des passagers
 * 
 * @param passagers 
 */
void DataBase::sauvegarderPassagers(map<long, Passager*> passagers){
    remove("DB/passagers.txt");

    // Ouvre le fichier en écriture, se positionne à la fin du fichier
    ofstream out("DB/passagers.txt", ios::app);

    for (map<long, Passager*>::iterator it = passagers.begin(); it != passagers.end(); it++) {
        stringstream str;
        str << it->second->getNom() << ";";
        str << it->second->getPrenom() << ";";
        str << it->second->getAge() << ";";
        str << it->second->getTitre() << ";";
        str << it->second->getNumPasseport();

        out << str.str() << endl;
    }
    out.close();

}

/**
 * @brief Récupère toutes les réservations du fichier dans un vecteur
 * 
 * @return vector<Reservation*> 
 */
vector<Reservation*> DataBase::listeReservations() {
    const int sz = 100;
    char buff[sz];
    ifstream in("DB/reservations.txt");
    vector<Reservation*> reservations;

    string str, str2;

    // Pour chaque ligne du fichier texte, on va créer une réservation et l'ajouter au vecteur
    while (in.get(buff, sz)) {
        in.get();
        str = buff;
        size_t start, end;
        end = 0;
        string out[10];
        int i = 0;

        // Permet de mettre dans le vecteur de string tout ce qui est entre ';'
        while ((start = str.find_first_not_of(';', end)) != std::string::npos) {
            end = str.find(';', start);
            out[i] = (str.substr(start, end - start));
            i++;
        }

        // Utilise le vecteur de strings pour créer un passager
        string nom, prenom, titre;
        bool conf;
        if (out[3] == "0") {
            conf = false;
        } else {
            conf = true;
        }
        Reservation *reservation = new Reservation(stol(out[0]), stol(out[1]), stol(out[2]), conf);
        reservations.push_back(reservation);
    }
    
    in.close();

    // Retourne la liste des passagers
    return reservations;
}

/**
 * @brief Ajoute une réservation à la suite du fichier
 * 
 * @param res 
 */
void DataBase::ajouterReservation(Reservation *res){
    // Ouvre le fichier en écriture, se positionne à la fin du fichier
    ofstream out("DB/reservations.txt", ios::app);
    
    stringstream str;
    str << res->getNumReservation() << ";";
    str << res->getNumPasseport() << ";";
    str << res->getNumVol() << ";";
    if (res->getConfirmation()) {
        str << 1;
    } else {
        str << 0;
    }
    

    out << str.str() << endl;

}

/**
 * @brief Efface le fichier reservations, pour pouvoir le reprendre à 0 avec les nouvelles données
 * 
 */
void DataBase::resetReservations(){
    // Efface le fichier pour le reprendre à 0
    remove("DB/reservations.txt");
}