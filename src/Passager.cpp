#include "../headers/Passager.h"

Passager::Passager(string nom, string prenom, int age, string titre, long numPasseport) {
    this->nom = nom;
    this->prenom = prenom;
    this->age = age;
    this->titre = titre;
    this->numPasseport = numPasseport;
}

Passager::~Passager() {
}

string Passager::getNom() const {
    return nom;
}

void Passager::setNom(const string nom) {
    this->nom = nom;
}

string Passager::getPrenom() const {
    return prenom;
}

void Passager::setPrenom(const string prenom) {
    this->prenom = prenom;
}

int Passager::getAge() const {
    return age;
}

void Passager::setAge(const int age) {
    this->age = age;
}

string Passager::getTitre() const {
    return titre;
}

void Passager::setTitre(const string titre) {
    this->titre = titre;
}

void Passager::setNumPasseport(const long numPasseport) {
    this->numPasseport = numPasseport;
}

long Passager::getNumPasseport() const {
    return this->numPasseport;
}

vector<Reservation*> Passager::getReservations() const {
    return this->reservations;
}

void Passager::setReservations(vector<Reservation*> reservations) {
    this->reservations = reservations;
}


string Passager::afficher() const {
    stringstream str;

    str << this->titre << " " << this->nom << " " << this->prenom << " ";
    str << this->age << " ans, ";
    str << "passeport : " << this->numPasseport;

    return str.str();
}

/**
 * @brief Méthode pour réserver un vol par un passager
 * 
 * @param vols : liste des vols existants
 * @param reservations : liste des reservations existantes. Modifié en sortie
 */
void Passager::reserverVol(map<long, Vol*> vols, map<long, Reservation*> &reservations) {
    long idSaisi;

    cout << "Quel vol voulez-vous réserver ?" << endl;
    this->afficherVols(vols);
    cout << "Saisissez son numéro de vol : ";
    long id;
    cin >> id;
    if (vols.find(id) == vols.end()) {
        cerr << "Vol non existant..." << endl;

    } else {
        vols[id]->getDate();
        idSaisi = Reservation::getNextNumReservation(reservations);
        Reservation *reservation = new Reservation(idSaisi, this->getNumPasseport(), vols[id]->getNumVol(), false);
        reservations[idSaisi] = reservation;
        
        cout << "Le vol a bien été réservé." << endl;

        this->ajouterReservation(reservation);
    }

}

/**
 * @brief Permet d'ajouter une réservation au vecteur des réservations d'un passager
 * 
 * @param reservation 
 */
void Passager::ajouterReservation(Reservation* reservation) {
    this->reservations.push_back(reservation);
}

/**
 * @brief Permet de supprimer une réservation au vecteur des réservations d'un passager
 * 
 * @param reservation 
 */
void Passager::supprimerReservation(Reservation* reservation) {
    long unsigned int i;
    for (i = 0; i < this->reservations.size(); i++) {
        if (reservation->getNumReservation() == this->reservations[i]->getNumReservation()) {
            break;
        }
        
    }

    this->reservations.erase(reservations.begin() + i);
}

/**
 * @brief Permet de lister les réservations d'un passager
 * 
 */
void Passager::listerReservations() const {
    for (size_t i = 0; i < this->reservations.size(); i++) {
        cout << this->reservations[i]->afficher() << endl;
    }
}

/**
 * @brief Permet de confirmer une réservation
 * 
 * @param reservations : liste des reservations existantes. Modifié en sortie
 */
void Passager::confirmerReservation(map<long, Reservation*> &reservations) {
    long idSaisi;

    cout << "Quelle réservation confirmer ?" << endl;
    this->listerReservations();
    cout << "Saissez son numéro de réservation : ";
    cin >> idSaisi;

    // On regarde d'abord si la réservation existe
    if (reservations.find(idSaisi) == reservations.end()) {
        cout << "Mauvais id" << endl;
    } else {
        // Puis, si elle appartient bien au passager connecté
        if (reservations[idSaisi]->getNumPasseport() != this->numPasseport) {
            cout << "Ce n'est pas votre réservations !" << endl;
        } else {
            // Enfin, on la valide
            reservations[idSaisi]->setConfirmation(true);
            cout << "La réservation a bien été confirmée" << endl;
        }
    }

}

/**
 * @brief Permet d'annuler une réservation
 * 
 * @param reservations : liste des reservations existantes. Modifié en sortie
 * @return int 0 si ok, 1 si opération annulée
 */
int Passager::annulerReservation(map<long, Reservation*> &reservations) {
    long idSaisi;

    cout << "Quelle réservation annuler ?" << endl;
    this->listerReservations();
    cout << "Saissez son numéro de réservation (0 pour retour) : ";
    cin >> idSaisi;

    if (idSaisi == 0) {
        cout << "Opération annulée" << endl;
        return 1;
    }
    

    // On regarde d'abord si la réservation existe
    if (reservations.find(idSaisi) == reservations.end()) {
        cout << "Mauvais id" << endl;
    } else {
        // Puis, si elle appartient bien au passager connecté
        if (reservations[idSaisi]->getNumPasseport() != this->getNumPasseport()) {
            cout << "Ce n'est pas votre réservations !" << endl;
        } else {
            // Enfin, on la supprime
            this->supprimerReservation(reservations.at(idSaisi));
            reservations.erase(idSaisi);
            cout << "La réservation a bien été annulée" << endl;
        }
    }

    return 0;

}
/**
 * @brief Permet, par un prompt de vérifier l'existence d'une réservation et d'en afficher les détails
 * 
 * @param reservations : liste des réservations
 */
void Passager::verifierExistenceReservation(map<long, Reservation*> reservations) const {
    long idSaisi;

    cout << "Veuillez saisir l'id d'une réservation pour vérifier son existence : " << endl;
    cin >> idSaisi;

    if (reservations.find(idSaisi) == reservations.end()) {
        cout << "Cette réservation n'existe pas" << endl;
    } else {
        if (reservations[idSaisi]->getNumPasseport() != this->getNumPasseport()) {
            cout << "Ce n'est pas votre réservation" << endl;
        } else {
            cout << "Cette réservation existe !" << endl << "Voici ses détails : " << endl;
            cout << reservations[idSaisi]->afficher() << endl;
        }
    }
}