#include "../headers/Personne.h"

/**
 * @brief Permet d'afficher la liste des vols
 * 
 * @param vols : liste des vols
 */
void Personne::afficherVols(map<long, Vol*> vols) const {
    // Itérateur sur la map
    map<long, Vol*>::iterator it;

    it = vols.begin();
    cout << "Liste des vols enregistrés :" << endl << endl;
    while (it != vols.end()) {
        cout << "Numéro vol : ";
        cout << it->second->getNumVol() << endl;
        cout << "Nombre places max : ";
        cout << it->second->getNbPlacesMax() << endl;
        cout << "Ville de départ : ";
        cout << it->second->getDestination()->getVilleDepart() << endl;
        cout << "Ville d'arrivée : ";
        cout << it->second->getDestination()->getVilleArrive() << endl;
        cout << "Date : ";
        cout << it->second->getDate()->toString() << endl;
        cout << "Prix : ";
        cout << it->second->getPrix() << endl << endl;

        it++;
    }
    cout << endl << "Fin de la liste des vols enregistrés :" << endl;
}

/**
 * @brief Permet, par un prompt, de vérifier l'existence d'un vol par son id, puis d'en afficher les détails
 * 
 * @param vols : liste des vols
 */
void Personne::verifierExistenceVol(map<long, Vol*> vols) const {
    long idSaisi;

    cout << "Veuillez saisir l'id d'un vol pour vérifier son existence : " << endl;
    cin >> idSaisi;

    if (vols.find(idSaisi) == vols.end()) {
        cout << "Ce vol n'existe pas" << endl;
    } else {
        cout << "Ce vol existe !" << endl << "Voici ses détails : " << endl;
        cout << vols[idSaisi]->afficher() << endl;
    }
}
