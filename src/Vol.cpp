#include "../headers/Vol.h"

Vol::Vol(long numVol, int nbPlacesMax, Destination *destination, Date *date, float prix) {
    this->numVol = numVol;
    this->nbPlacesMax = nbPlacesMax;
    this->destination = destination;
    this->date = date;
    this->prix = prix;
}

Vol::~Vol() {
}

long Vol::getNumVol() const {
    return numVol;
}

void Vol::setNumVol(const long numVol) {
    this->numVol = numVol;
}

int Vol::getNbPlacesMax() const {
    return nbPlacesMax;
}
void Vol::setNbPlacesMax(const int nbPlacesMax) {
    this->nbPlacesMax = nbPlacesMax;
}

Destination* Vol::getDestination() const {
    return destination;
}

void Vol::setDestination(Destination *destination) {
    this->destination = destination;
}

float Vol::getPrix() const {
    return prix;
}

void Vol::setPrix(const float prix) {
    this->prix = prix;
}

Date* Vol::getDate() const {
    return date;
}

void Vol::setDate(Date *date){
    this->date = date;
}

/**
 * @brief Retourne les données d'un vol
 * 
 * @return string 
 */
string Vol::afficher() const {
    stringstream str;
    str << "Numéro : " << this->numVol << " ";
    str << "Places : " << this->nbPlacesMax << " ",
    str << "Trajet : " << this->destination->getVilleDepart() << " -> "; 
    str << this->destination->getVilleArrive() << " "; 
    str << this->date->toString() << " "; 
    str << this->prix << "€";
    return str.str();
}