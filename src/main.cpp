#include <iostream>
#include "../headers/Personne.h"
#include "../headers/Passager.h"
#include "../headers/Vol.h"
#include "../headers/Destination.h"
#include "../headers/Reservation.h"
#include "../headers/Administrateur.h"
#include "../headers/Date.h"
#include "../headers/Menus.h"


int main() {
    cout << "Bienvenue sur le serveur de gestion de Vol !" << endl;

    // Bannière
    cout << 
        "                       ___" << endl << 
        "                       \\  \\" << endl << 
        "                        \\ `\\" << endl << 
        "     ___                 \\  \\" << endl << 
        "    |    \\                \\  `\\" << endl << 
        "    |_____\\                \\    \\" << endl << 
        "    |______\\                \\    `\\" << endl << 
        "    |       \\                \\     \\" << endl << 
        "    |      __\\__---------------------------------._." << endl << 
        "  __|---~~~__o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_[][\\__" << endl << 
        " |___    GESTION D'AEROPORT   /~      )                \\__" << endl << 
        "     ~~~---..._______________/      ,/_________________/" << endl << 
        "                            /      /" << endl << 
        "                           /     ,/" << endl << 
        "                          /     /" << endl << 
        "                         /    ,/" << endl << 
        "                        /    /" << endl << 
        "                       //  ,/" << endl << 
        "                      //  /" << endl << 
        "                     // ,/" << endl << 
        "                    //_/" << endl
    << endl;

    cout << "Êtes vous un admin ? (y/n)" << endl;

    string choix;
    do {
        cin >> choix;
    } while (choix != "Y" && choix != "y" && choix != "N" && choix != "n");

    int menu;

    if (choix == "Y" || choix == "y") {
        menu = menuAdmin();
    } else {
        menu = menuUser();
    }
    
    if (menu != 0) {
        cerr << "Une erreur est survenue..." << endl;
    } else {
        cout << "Fin du programme." << endl;
    }
    
}