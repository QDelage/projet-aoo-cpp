#include "../headers/Reservation.h"

Reservation::Reservation(long numReservation, long numPasseport, long numVol, bool confirmation) {
    this->numReservation = numReservation;
    this->numPasseport = numPasseport;
    this->numVol = numVol;
    this->confirmation = confirmation;
}

Reservation::~Reservation() {
}

long Reservation::getNumReservation() const {
    return this->numReservation;
}

void Reservation::setNumReservation(const long numReservation){
    this->numReservation = numReservation;
}

long Reservation::getNumPasseport() const {
    return this->numPasseport;
}

void Reservation::setNumPasseport(const long numPasseport){
    this->numPasseport = numPasseport;
}

long Reservation::getNumVol() const {
    return this->numVol;
}

void Reservation::setNumVol(const long numVol){
    this->numVol = numVol;
}

bool Reservation::getConfirmation() const {
    return this->confirmation;
}

void Reservation::setConfirmation(const bool confirmation){
    this->confirmation = confirmation;
}

/**
 * @brief Retourne les données de la réservation
 * 
 * @return string : données concaténées et nommées
 */
string Reservation::afficher() const {
    stringstream str;
    str << "Réservation : " << this->numReservation;
    str << " ";
    str  << "Passeport : " << this->numPasseport;
    str << " ";
    str << "Vol : " << this->numVol;
    str << " ";
    str << "Confirmation : " << this->confirmation;

    return str.str();
}