#include "../headers/Destination.h"

Destination::Destination(string villeDepart, string villeArrive) {
    this->villeDepart = villeDepart;
    this->villeArrive = villeArrive;
}

Destination::~Destination() {
}

string Destination::getVilleDepart() const {
    return this->villeDepart;
}

void Destination::setVilleDepart(const string villeDepart){
    this->villeDepart = villeDepart;
}

string Destination::getVilleArrive() const {
    return this->villeArrive;
}

void Destination::setVilleArrive(const string villeArrive){
    this->villeArrive = villeArrive;
}