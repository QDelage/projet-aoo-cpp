# Gestion d'aéroport

Projet simple de CPP de gestion  de vols en ligne de commande, dans le cadre de l'UE AOO - CPP de ma L3 Informatique.

Fonctionnalités : gestion des vols, passagers, réservations, etc.

## Pour commencer

Simplement utilisez le makefile et exécutez le programme en ligne de commande.

L'identifiant administrateur est admin:admin

## Auteur

* **Quentin Delage** - *Initial work* - [QDelage](https://gitlab.com/QDelage)

## License

Ce projet est disponible sous license MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails
