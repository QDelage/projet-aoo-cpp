#
# In order to execute this "Makefile" just type "make"
#	A. Delis (ad@di.uoa.gr)
#

OBJS	= src/Administrateur.o src/DB.o src/Date.o src/Destination.o src/Menus.o src/Passager.o src/Personne.o src/Reservation.o src/Vol.o src/main.o
SOURCE	= src/Administrateur.cpp src/DB.cpp src/Date.cpp src/Destination.cpp src/Menus.cpp src/Passager.cpp src/Personne.cpp src/Reservation.cpp src/Vol.cpp src/main.cpp
HEADER	= headers/Administrateur.h headers/DB.h headers/Date.h headers/Destination.h headers/Menus.h headers/Passager.h headers/Personne.h headers/Reservation.h headers/Vol.h
OUT	= gestionDeVol.exe
CC	 = g++
FLAGS	 = -g -c -Wall
LFLAGS	 = -Wall
# -g option enables debugging mode 
# -c flag generates object code for separate files


all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS) -std=c++14


# create/compile the individual files >>separately<<
%.o: %.cpp
	@$(CC) -o $@ $< $(FLAGS) -std=c++14

# clean house
clean:
	rm -f $(OBJS) $(OUT) *.o