#if !defined(DATE)
#define DATE

#include <string>

using namespace std;

class Date {
private:
    int jour; // jour : 1 à 31
    int mois; // mois : 0 à 11
    int annee; // année

    int heure; // heures : 0 à 23
    int min; // minutes : 0 à 59

public:
    // Renvoie une exception de type int
    // L'entier (de 1 à 5) représente le champ en erreur
    Date(int jour, int mois, int annee, int min, int heure);
    ~Date();

    // Getters et Setters

    int getJour() const;
    void setJour(const int);
    int getMois() const;
    void setMois(const int);
    int getAnnee() const;
    void setAnnee(const int);

    int getMin() const;
    void setMin(const int);
    int getHeure() const;
    void setHeure(const int);

    string toString() const;

    /**
     * @brief Permet de savoir si une date est bien postérieure à la date actuelle
     * 
     * @return true : elle est dans le futur
     * @return false : elle est passée
     */
    bool estUneDatePosterieure() const;
};


#endif // DATE