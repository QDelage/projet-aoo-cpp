#if !defined(DB)
#define DB

#include <iostream>
#include <fstream>
#include "Vol.h"
#include <sstream>
#include <vector>
#include "Passager.h"
#include "Reservation.h"

// forward declaration
class Administrateur;

using namespace std;

/**
 * @brief Fonctions / procédures qui gèrent les données persistantes sur le disque
 * 
 */
namespace DataBase {
    /**
     * @brief Permet d'obtenir la liste de tous les admins de la DB
     * 
     * @return vector<Administrateur> : liste des admins autorisés
     */
    vector<Administrateur> getAllAdmins();

    /**
     * @brief Permet de récupérer les vols existants
     * 
     * @return vector<Vol*> : vols de la DB
     */
    vector<Vol*> listVol();

    /**
     * @brief Ajoute un vol à la suite du fichier
     * 
     * @param vol
     */
    void ajouterVol(Vol *vol);

    /**
     * @brief Écrase le fichier actuel avec les infos des vols de la map
     * 
     * @param vols 
     */
    void sauvegarderVols(map<long, Vol*>);

    /**
     * @brief Efface le fichier vols, pour pouvoir le reprendre à 0 avec les nouvelles données
     * 
     */
    void resetVols();

    /**
     * @brief Permet de récupérer un vecteur des passagers existants en DB
     * 
     * @return vector<Passager*> 
     */
    vector<Passager*> listPassagers();

    /**
     * @brief Permet de récupérer une map des passagers existants en DB
     * 
     * @return map<long, Passager*> 
     */
    map<long, Passager*> listePassagers();

    /**
     * @brief Récupère les réservations d'un passager depuis la DB
     * 
     * @param numPasseport : ID du passager
     * @return vector<Reservation*> 
     */
    vector<Reservation*> getReservationsParPassager(long numPasseport);

    /**
     * @brief Permet de choisir son passager parmi la DB
     * S'il n'éxiste pas, retourne NULL
     * 
     * @return Passager* 
     */
    Passager* choixPassager();

    /**
     * @brief Permet de créer un passager.
     * Demande ses informations en prompt.
     * Écrit automatiquement ses infos dans la DB.
     * 
     * @return Passager* : Pointeur vers le nouveau passager
     */
    Passager* creerPassager();

    /**
     * @brief Ajoute un à un les passagers au fichier des passagers
     * 
     * @param passagers 
     */
    void sauvegarderPassagers(map<long, Passager*>);

    /**
     * @brief Récupère toutes les réservations du fichier dans un vecteur
     * 
     * @return vector<Reservation*> 
     */
    vector<Reservation*> listeReservations();

    /**
     * @brief Ajoute une réservation à la suite du fichier
     * 
     * @param res 
     */
    void ajouterReservation(Reservation *res);

    /**
     * @brief Efface le fichier reservations, pour pouvoir le reprendre à 0 avec les nouvelles données
     * 
     */
    void resetReservations();

} // namespace DataBase


#endif // DB
