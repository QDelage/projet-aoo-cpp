#if !defined(PERSONNE)
#define PERSONNE

#include <map>
#include "Vol.h"
#include "iostream"

using namespace std;

class Personne {
private:
    /* data */
public:

    /**
     * @brief Permet d'afficher la liste des vols
     * 
     * @param vols : liste des vols
     */
    virtual void afficherVols(map<long, Vol*> vols) const;

    /**
     * @brief Permet, par un prompt, de vérifier l'existence d'un vol par son id, puis d'en afficher les détails
     * 
     * @param vols : liste des vols
     */
    virtual void verifierExistenceVol(map<long, Vol*> vols) const;
};

#endif // PERSONNE
