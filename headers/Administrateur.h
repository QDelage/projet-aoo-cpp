#if !defined(ADMINISTRATEUR)
#define ADMINISTRATEUR

#include <string>
#include "Personne.h"
#include "Passager.h"
#include "DB.h"

using namespace std;

class Administrateur : public Personne{
private:
    string id;
    string mdp;
public:
    Administrateur(string id, string mdp);
    ~Administrateur();

    // Getters et Setters

    string getId() const ;
    void setId(const string id);
    string getMdp() const ;
    void setMdp(const string mdp);

    /**
     * @brief Permet d'ajouter un vol à la map
     * 
     * @param vols : map de vol, modifiée en sortie
     * @throws char* : exception si la date saisie est antérieure à la date actuelle (de la machine)
     */
    void ajouterVol(map<long, Vol*> *vols);

    /**
     * @brief Permet de modifier la date / heure d'un vol dont l'id doit être saisi manuellement
     * 
     * @param vols : map de tous les vols
     */
    void modifierDateHeureVol(map<long, Vol*> *vols);

    /**
     * @brief Permet d'afficher la liste des passagers par vol
     * 
     * @param vols 
     */
    void afficherPassagersParVols(map<long, Passager*> passagers, map<long, Vol*> vols) const;

    /**
     * @brief Permet de tester l'egalité de deux Administrateurs
     * 
     * @param admin : l'autre administrateur
     * @return true : id ET mdp sont égaux
     * @return false : sinon
     */
    bool operator==(const Administrateur &admin) const;

    /**
     * @brief Permet de créer un administrateur
     * 
     * @return Administrateur : les id et mdp doivent être non vide, sinon c'est une erreur de connexion
     */
    static Administrateur connexion() {

        vector<Administrateur> admins = DataBase::getAllAdmins();

        string id, mdp;

        cout << "Veuillez saisir votre identifiant : " << endl;
        cin >> id;


        cout << "Veuillez saisir votre mot de passe : " << endl;
        cin >> mdp;

        Administrateur adm(id, mdp);

        for (size_t i = 0; i < admins.size(); i++) {
            if (adm == admins[i]) {
                cout << "ADMIN ACCESS GRANTED" << endl;
                return adm;
            }
        }
        

        cerr << "ADMIN ACCESS REFUSED" << endl;
        return Administrateur("", "");
    }

};


#endif // ADMINISTRATEUR