#if !defined(PASSAGER)
#define PASSAGER

#include <string>
#include "Personne.h"
#include "Reservation.h"

using namespace std;

class Passager : public Personne {
private:
    string nom;
    string prenom;
    int age;
    string titre;
    long numPasseport;
    vector<Reservation*> reservations;
public:
    Passager(string nom, string prenom, int age, string titre, long numPasseport);
    virtual ~Passager();

    // Getters et setters
    
    string getNom() const;
    void setNom(const string nom);
    string getPrenom() const;
    void setPrenom(const string prenom);
    int getAge() const;
    void setAge(const int age);
    string getTitre() const;
    void setTitre(const string titre);
    long getNumPasseport() const;
    void setNumPasseport(const long numPasseport);
    vector<Reservation*> getReservations() const;
    void setReservations(vector<Reservation*> reservations);

    string afficher() const;

    /**
     * @brief Méthode pour réserver un vol par un passager
     * 
     * @param vols : liste des vols existants
     * @param reservations : liste des reservations existantes. Modifié en sortie
     */
    void reserverVol(map<long, Vol*> vols, map<long, Reservation*> &reservations);

    /**
     * @brief Permet d'ajouter une réservation au vecteur des réservations d'un passager
     * 
     * @param reservation 
     */
    void ajouterReservation(Reservation* reservation);

    /**
     * @brief Permet de supprimer une réservation au vecteur des réservations d'un passager
     * 
     * @param reservation 
     */
    void supprimerReservation(Reservation* reservation);

    /**
     * @brief Permet de lister les réservations d'un passager
     * 
     */
    void listerReservations() const;

    /**
     * @brief Permet de confirmer une réservation
     * 
     * @param reservations : liste des reservations existantes. Modifié en sortie
     */
    void confirmerReservation(map<long, Reservation*> &reservations);

    /**
     * @brief Permet d'annuler une réservation
     * 
     * @param reservations : liste des reservations existantes. Modifié en sortie
     * @return int 0 si ok, 1 si opération annulée
     */
    int annulerReservation(map<long, Reservation*> &reservations);

    /**
     * @brief Permet, par un prompt de vérifier l'existence d'une réservation et d'en afficher les détails
     * 
     * @param reservations : liste des réservations
     */
    void verifierExistenceReservation(map<long, Reservation*> reservations) const;
};

#endif // PASSAGER