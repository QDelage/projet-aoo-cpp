#if !defined(DESTINATION)
#define DESTINATION
#include <string>

using namespace std;

class Destination {
private:
    string villeDepart;
    string villeArrive;
public:
    Destination(const string villeDepart = "Inconnue", const string villeArrive = "Inconnue");
    ~Destination();

    // Getters et setters

    string getVilleDepart() const;
    void setVilleDepart(const string villeDepart);
    string getVilleArrive() const;
    void setVilleArrive(const string villeArrive);
};


#endif // DESTINATION