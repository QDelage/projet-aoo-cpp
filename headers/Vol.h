#if !defined(VOL)
#define VOL

#include "Destination.h"
#include "Date.h"
#include <sstream>

class Vol {
private:
    long numVol;
    int nbPlacesMax;
    Destination *destination;
    Date *date;
    float prix;

public:
    Vol(long numVol, int nbPlacesMax, Destination *destination, Date *date, float prix);
    ~Vol();

    // Getters et setters

    long getNumVol() const;
    void setNumVol(const long numVol);
    int getNbPlacesMax() const;
    void setNbPlacesMax(const int nbPlacesMax);
    Destination* getDestination() const;
    void setDestination(Destination *destination);
    float getPrix() const;
    void setPrix(const float prix);
    Date* getDate() const;
    void setDate(Date *date);
    string afficher() const;
};


#endif // VOL