#if !defined(RESERVATION)
#define RESERVATION

#include <vector>
#include <climits>
#include <map>
#include <string>
#include <sstream>

using namespace std;

class Reservation {
private:
    long numReservation;
    long numPasseport;
    long numVol;
    bool confirmation;
public:
    Reservation(long numReservation, long numPasseport, long numVol, bool confirmation);
    ~Reservation();

    // Getters et Setters

    long getNumReservation() const;
    void setNumReservation(const long numReservation);
    long getNumPasseport() const;
    void setNumPasseport(const long numPasseport);
    long getNumVol() const;
    void setNumVol(const long numVol);
    bool getConfirmation() const;
    void setConfirmation(const bool confirmation);
    string afficher() const;

    /**
     * @brief Permet d'obtenir le prochain numéro de réservation disponible (de 1 au maximum)
     * 
     * @param reservations 
     * @return const long 
     */
    static long getNextNumReservation(const map<long, Reservation*> reservations) {
        for (long i = 1; i < LONG_MAX; i++) {
            if (reservations.find(i) == reservations.end()) {
                return i;
            }
        }
        return -1;
    }
};


#endif // RESERVATION