#if !defined(MENU)
#define MENU

#include "string"
#include "iostream"
#include "../headers/Passager.h"
#include "../headers/Vol.h"
#include "../headers/Destination.h"
#include "../headers/Reservation.h"
#include "../headers/Administrateur.h"
#include "../headers/Date.h"

/**
 * Menu de l'administrat·eur·rice
 * Retourne 0 si tout se passe bien
 */
int menuAdmin();

/**
 * Menu de l'utilisat·eur·rice
 * Retourne 0 si tout se passe bien
 */
int menuUser();

#endif // MENU